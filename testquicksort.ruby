require 'benchmark'
include Benchmark

class SortTest

	attr_accessor :test, :std

	def initialize(num,type="R")
		@type = type
		@std = Array.new(0)
		if type.eql?('R')
			num.times do |i|
				@std[i] = (num * 100 * rand).round
			end
		else
			num.times do |i|
				@std[i] = num - i
			end
		end
	end
	
	def reset
		@test = @std.dup
	end
  


	def quickSort1(low = 0, high = @test.length-1)
		return if (low >= high) || (low < 0) 
		if low + 1 == high
			if @test[low] > @test[high]
				@test[low], @test[high] = @test[high], @test[low]
			end
			return
		end
  
  		p = partition(low, high)
		
		quickSort1(low, p - 1)
		quickSort1(p + 1, high)
	end
	
	def partition(lo,hi)
		mid = ((lo + hi)/2).floor
		if @test[mid] <= @test[hi]
		   @test[mid], @test[hi] = @test[hi], @test[mid]
		end
		pivot = hi
		j = hi - 1; i = lo

		while i < j 
			while @test[i] < @test[pivot] do i += 1 end
			while @test[j] > @test[pivot] do j -= 1 end
			
			if i < j
				@test[i], @test[j] = @test[j], @test[i]
				i += 1
			end
		end

		@test[pivot], @test[i] = @test[i], @test[pivot]
		return i
	end

end




    def quickSort2(arr, low, high)
        if(low < high)
            pivot = partition(arr, low, high)
            quickSort2(arr, low, pivot)
            quickSort2(arr, pivot + 1, high)
        end
    end

    def partition(arr, low, high)
        pivot = arr[low]
        i = low
        j = high

        while(i < j)
            while arr[i].to_i <= pivot.to_i and i < high
                i += 1
            end

            while arr[j].to_i > pivot.to_i and j > low
                j -= 1
            end

            if(i < j)
                temp = arr[i]
                arr[i] = arr[j]
                arr[j] = temp
            end
        end

        arr[low] = arr[j]
        arr[j] = pivot

        return j
    end



repetition = 100
["RevSeq","Random"].each do |typ|
4.times do |iteration|
[100,1000,10000,100000].each do |siz|
t = SortTest.new(siz,type=typ)

bmbm(25) do |test|
	
	
	STDERR.puts "%12s, %s, %5d, %2d" % ["1,Quick1",typ,siz,iteration]
	test.report("%s,%s, %5d, %2d," % ["1,Quick1",typ,siz,iteration]) do
		repetition.times do
			t.reset
			# puts t.test.inspect
			t.quickSort1
			# puts t.test.inspect
		end
    end
	
	STDERR.puts "%12s, %s, %5d, %2d" % ["2,Quick2",typ,siz,iteration]
	test.report("%s,%s, %5d, %2d," % ["2,Quick2",typ,siz,iteration]) do
		repetition.times do
			t.reset
			# puts t.test.inspect
			t.quickSort1
			# puts t.test.inspect
		end
    end
end

end
end
end