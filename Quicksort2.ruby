class QuickSortExample

    def quickSort2(arr, low, high)
        if(low < high)
            pivot = partition(arr, low, high)
            quickSort2(arr, low, pivot)
            quickSort2(arr, pivot + 1, high)
        end
    end

    def partition(arr, low, high)
        pivot = arr[low]
        i = low
        j = high

        while(i < j)
            while arr[i].to_i <= pivot.to_i and i < high
                i += 1
            end

            while arr[j].to_i > pivot.to_i and j > low
                j -= 1
            end

            if(i < j)
                temp = arr[i]
                arr[i] = arr[j]
                arr[j] = temp
            end
        end

        arr[low] = arr[j]
        arr[j] = pivot

        return j
    end

    def printArray(arr)
        for i in 0...arr.length do
            print(arr[i].to_s + " ")
        end
        puts("")
    end
end

obj = QuickSortExample.new

arr = [15, 3, 12, 6, -9, 9, 0]
print("Before Sorting: ")
obj.printArray(arr)

obj.quickSort2(arr, 0, arr.length - 1)
print("After Sorting: ")
obj.printArray(arr)